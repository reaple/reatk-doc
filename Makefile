# -*- makefile-gmake -*-
# ----------------------------------------------------------------------
#
# Makefile for org-mode export and publishing.
# 
# Copyright (C) 2015 Nicolas Berthier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# ------------------------------------------------------------------------------

pubdir ?= www
targets = index user-manual tech-refs
no_pdf = index

# remote_user = nberthier
# remote_host = scm.gforge.inria.fr
# remote_dir  = /home/groups/reatk/htdocs
local_dir  = /home/nico/www/nberth.space/htdocs/reatk

# ------------------------------------------------------------------------------

SHELL=/bin/bash
THISDIR = $(dir $(lastword $(MAKEFILE_LIST)))

RSYNC = rsync -rvz
RSYNC_DELETE ?= no
ifeq ($(RSYNC_DELETE),yes)
  RSYNC += --delete
endif

EMACS = emacs
#--directory ~/.emacs.d/site-lisp/org-mode/lisp

HAS_ORG = $(shell command -v emacs >/dev/null && ( $(EMACS) --batch	\
	--eval "(require 'org)" >&/dev/null && echo yes ) || echo no)

# ---

alldeps = $(wildcard etc/*.org) $(wildcard etc/*.el)
orgs  = $(addsuffix .org,$(targets))
pdfs  = $(addsuffix .pdf,$(filter-out $(no_pdf),$(targets)))
htmls = $(addsuffix .html,$(filter-out $(no_html),$(targets)))
bibs  = $(addsuffix .bib,$(targets))

# ------------------------------------------------------------------------------

.PHONY: default publish
default: $(pdfs)
publish: publish-all

.PHONY: clean
clean: force
	-:$(addprefix ; latexmk -C ,$(targets))
	rm -f $(addsuffix .tex,$(targets)) $(addsuffix .html,$(targets)) \
	  *~ fig/tikz/* *.auxlock *.log *.bbl *~ \
	  *.png *.tikz *.bib \
	  local_bib.html local.html local.txt \
	  theindex.inc

.PHONY: install-org
install-org:
	emacs --batch \
	      --eval "(package-initialize)" \
	      --eval "(package-refresh-contents)" \
	      --eval "(package-install 'org)"

# ------------------------------------------------------------------------------

ifneq ($(HAS_ORG),yes)

%:
	@echo "Exporting documentations requires org-mode!" >/dev/stderr;
	@echo "Typing \`make install-org' should automatically install it"\
	      "if the version of the default emacs >= 24." >/dev/stderr;
	@echo "Please visit \`http://orgmode.org/' (or contact the maintainer"\
	      "of the project) otherwise." >/dev/stderr;

else				#HAS_ORG=yes

define ORGEXPORT
	$(EMACS) --batch \
	      --no-site-file \
	      --funcall toggle-debug-on-error \
	      --funcall package-initialize \
	      --eval "(require 'org)" \
	      --eval "(require 'ob-latex)" \
	      $(foreach o,$(1), \
	      --visit $(o) \
	      --load "etc/setup.el" \
	      --funcall $(2)) 2&>/dev/stdout
endef

define PUBLISH_PROJECT
	$(EMACS) --batch \
	      --no-site-file \
	      --funcall package-initialize \
	      --funcall toggle-debug-on-error \
	      --eval "(require 'org)" \
	      --eval "(require 'ob-latex)" \
	      --eval "(setq my-publishing-p t)" \
	      --eval "(setq my-publish-dir \"$(pubdir)/\")" \
	      --eval "(setq my-doc-dir \"$(THISDIR)\")" \
	      --eval "(setq org-confirm-babel-evaluate nil)" \
	      --load "etc/setup.el" \
	      --load "etc/project.el" \
	      --eval "(org-publish-project \"$(1)\")"
endef

publish: publish-all

PROJS = $(addprefix publish-,all static pdf html data css imgs docs www)
.PHONY: $(PROJS)
$(PROJS): publish-%: $(orgs) $(alldeps)
	$(call PUBLISH_PROJECT,$*)

$(pdfs): %.pdf: %.org $(alldeps)
	$(call ORGEXPORT,$<,org-latex-export-to-pdf)

# XXX Brocken for now due to error in `org-publish-cache-get'.
$(htmls): %.html: %.org $(alldeps)
	$(call ORGEXPORT,$<,org-html-export-to-html)

$(PROJS) $(pdfs) $(htmls): $(bibs)

endif				#HAS_ORG=yes

# ------------------------------------------------------------------------------

tech-refs.bib: bib/publications.bib bib/tech-refs.bib
user-manual.bib: bib/publications.bib bib/user-manual.bib
index.bib: bib/publications.bib
$(bibs): %.bib:
	cat $+ > $@

# ------------------------------------------------------------------------------

.PHONY: rsync rsync-dry rsync-dry-local
rsync: publish-all rsync-dry
rsync-dry: force
	$(RSYNC) --exclude "*~" \
	  "$(pubdir)/" "$(remote_user)@$(remote_host):$(remote_dir)/"
rsync-dry-local: force
	$(RSYNC) --exclude "*~" "$(pubdir)/" "$(local_dir)/"

.PHONY: force
force:

# ------------------------------------------------------------------------------
