(define-generic-mode 'ctrln-mode
  ;; (list "(* " " *)")
  ()
  '("enum" "not" "and" "or" "xor" "eq" "in" "if" "then" "else") ; keywords
  '(
    ;; Boolean constants
    ("\\<true\\|false\\>"
     . 'font-lock-constant-face)

    ;; Predefined type names
    ("\\<bool\\|int\\|real\\|uint\\|sint\\>"
     . 'font-lock-type-face)

    ;; Keywords
    ("!\\(typedef\\|state\\|input\\|output\\|local\\|controllable\\)\\>"
     . 'font-lock-keyword-face)
    ("!\\(transition\\|definition\\)\\>" 
     . 'font-lock-keyword-face)
    ("!\\(invariant\\|initial\\|final\\|assertion\\|reachable\\|attractive\\|value\\)\\>"
     . 'font-lock-keyword-face)
    ("#\\|,\\|;\\|<=?\\|>=?\\|<>\\|:\\|?\\|="
     . 'font-lock-keyword-face)
    ("\{\\|\}\\|\(\\|\)\\|\[\\|\]"
     . 'font-lock-keyword-face)

    ;; ;; Identifiers
    ;; ("[A-Z][A-Za-z[:digit:]_@!]*"
    ;;  . 'font-lock-constant-face)

    ;; Identifiers
    ("[A-Za-z_@][A-Za-z[:digit:]_@!]*"
     . 'font-lock-variable-name-face)

    ;; Numbers
    ("[[:digit:]]*\\.?[[:digit:]]+\\([eE][+-]?[[:digit:]]+\\)?"
     . 'font-lock-constant-face)
    ;; ("\,\\|-\\|\\+\\|\\*\\|=" . 'font-lock-keyword-face)
    )
  '("\\.ctrl[nfrs]")                    ; filename suffixes
  nil                                   ; extra function hooks
  "Major mode for Controllable-Nbac highlighting.")
