;; -*- coding: utf-8 -*-
;; ----------------------------------------------------------------------------

;; ----------------------------------------------------------------------------
;; Helper for defining publishing info:

(require 'ox-publish)
;; (eval-after-load "ox-publish"
(setq org-publish-project-alist;; )
      `(("html"
	 :base-directory ,my-doc-dir
	 :publishing-directory ,my-publish-dir
	 :publishing-function org-html-publish-to-html
	 :with-latex t
	 :html-inline-images t
	 :makeindex t
	 :include ("index.org"
		   "user-manual.org"
		   "tech-refs.org"
		   "theindex.org")
	 )
	("imgs"
	 :base-directory ,my-doc-dir
	 :base-extension "png"
	 :publishing-directory ,my-publish-dir
	 :publishing-function org-publish-attachment
	 ;; :include ("local_bib.html")
	 )
	("data"
	 :base-directory ,my-doc-dir
	 :base-extension "ctrl[nfprd]\\|lut"
	 :publishing-directory ,my-publish-dir
	 :publishing-function org-publish-attachment
	 )
	("css"
	 :base-directory ,my-doc-dir
	 :base-extension "css"
	 :recursive t
	 :publishing-directory ,my-publish-dir
	 :publishing-function org-publish-attachment
	 )
	("pdf"
	 :base-directory ,my-doc-dir
	 :publishing-directory ,my-publish-dir
	 :publishing-function org-latex-publish-to-pdf
	 :include ("user-manual.org"
		   "tech-refs.org")
	 :exclude "index\\.org"
	 )
	("docs" :components ("pdf" "data"))
	("static" :components ("imgs" "css" "data"))
	("www" :components ("html" "pdf" "static"))))

;; ----------------------------------------------------------------------------
