
(define-generic-mode 'bnf-mode
  (list "# ")
  nil                                   ; keywords
  '(
    ("^#.*$" . 'font-lock-comment-face) ; comments at start of line
    ;; ("^<.*?>" . 'font-lock-function-name-face) ; LHS nonterminals
    ("<[A-Za-z-']+>" . 'font-lock-function-name-face) ; other nonterminals
    ("'.'" . 'font-lock-string-face)          ; single character
    ("::=" . 'font-lock-keyword-face)         ; "goes-to" symbol
    ("\|\\|\(\\|\)\\|\{\\|\}\\|\\[\\|\\]\\|\,\\|-" . 'font-lock-keyword-face)
    )
  '("\\.bnf")                           ; filename suffixes
  nil                                   ; extra function hooks
  "Major mode for BNF highlighting.")
