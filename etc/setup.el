;; -*- coding: utf-8 -*-
;; ----------------------------------------------------------------------------
(require 'cl)                           ;for `lexical-let'
(require 'sh-script)
(require 'ox-latex)
(require 'ox-html)

;; (push (concat (org-find-library-dir "org") "../contrib/lisp/") load-path)
;; (add-to-list 'load-path
;; 	     (concat (org-find-library-dir "org") "../contrib/lisp/"))
;; (require 'org-bibtex)
(require 'ox-bibtex)			;in contrib

(let ((dir (file-name-directory load-file-name)))
  (load-file (expand-file-name "bnf-mode.el" dir))
  (load-file (expand-file-name "ctrln-mode.el" dir)))

(if (boundp 'my-publishing-p)
    (defun mk-local-var (var) var)
    (defun mk-local-var (var) (make-variable-buffer-local var)))

;; ----------------------------------------------------------------------------
;; Per-backend config.

(defmacro by-backend (&rest body)
  `(case org-export-current-backend ,@body))

;; ----------------------------------------------------------------------------
;; Helpers to export tikz figures:

;; (setq org-babel-latex-htlatex "htlatex")

(defmacro tikz-file-name (basename)
  `(by-backend (latex ,(concat basename ".tikz"))
	       (t ,(concat basename ".png"))))

;; (defmacro raw-file-on-export (&rest backends)
;;   `(or (case (if (boundp 'backend) (org-export-backend-name backend) nil)
;; 	 ((,@backends) "raw file")) "latex"))

(defun in-dir (file)
  (expand-file-name file (file-name-directory (or (buffer-file-name) "."))))

;; ----------------------------------------------------------------------------
;; To load locally-defined modes:
(add-to-list (mk-local-var 'load-path) (expand-file-name "./etc"))

(add-to-list (mk-local-var 'org-html-text-markup-alist)
	     '(code . "<span class=\"sf\">%s</span>"))
(add-to-list (mk-local-var 'org-html-text-markup-alist)
	     '(italic . "<span class=\"emph\">%s</span>"));; ))

(add-to-list (mk-local-var 'org-latex-text-markup-alist)
	     '(code . "\\textsf{%s}"));; ))

;; ----------------------------------------------------------------------------
;; Customizations for bibliography management:

;; (org-add-link-type
;;  "citet" 'ebib
;;  (lambda (path desc format)
;;    (cond ((eq format 'html)
;; 	  (cond
;; 	    ((and path desc) (format "<a href=\"%s\"%s>%s</a>"
;; 				     path attributes desc))
;; 	    (path (format "<a href=\"%s\"%s>%s</a>" path attributes path))))
;; 	 ((eq format 'latex) (format "\\citet{%s}" path)))))
 
;; (org-add-link-type
;;  "citep" 'ebib
;;  (lambda (path desc format)
;;    (cond ((eq format 'html)
;; 	  (cond
;; 	    ((and path desc) (format "<a href=\"%s\"%s>%s</a>"
;; 				     path attributes desc))
;; 	    (path (format "<a href=\"%s\"%s>%s</a>" path attributes path))))
;; 	 ((eq format 'latex) (format "\\citep{%s}" path)))))

;; (org-link-set-parameters
;;  "citep"
;;  :export (lambda (path desc backend)
;; 	   (cond
;; 	     ;; ((eq backend 'html)
;; 	     ;;  (format "<a href=\"%s\">%s</a>" path (or desc path)))
;; 	     ((eq backend 'latex)
;; 	      (format "\\citep{%s}" path)))))

;; ----------------------------------------------------------------------------
;; Customizations for LaTeX export:

;; XXX Really ugly... (to find u8chars.sty from interactive Emacs)
;; (setenv "TEXINPUTS" (concat (in-dir "tex") ":" (getenv "TEXINPUTS")))
;; (setenv "COLUMNS" "90")

;; (eval-after-load "ox-latex"
;;   '(progn
(add-to-list
 'org-latex-classes
 `("scrartcl"
   "\\documentclass{scrartcl}
    [NO-DEFAULT-PACKAGES]
    [PACKAGES]
    [EXTRA]"
   ("\\section{%s}" . "\\section*{%s}")
   ("\\subsection{%s}" . "\\subsection*{%s}")
   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
   ("\\paragraph{ %s}" . "\\paragraph*{%s}")
   ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
   ("\\subsubparagraph{%s}" . "\\subsubparagraph*{%s}")))
(add-to-list
 'org-latex-classes
 `("scrreprt"
   "\\documentclass{scrreprt}
    [NO-DEFAULT-PACKAGES]
    [PACKAGES]
    [EXTRA]"
   ("\\part{%s}" . "\\part*{%s}")
   ("\\section{%s}" . "\\section*{%s}")
   ("\\subsection{%s}" . "\\subsection*{%s}")
   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
   ("\\paragraph{%s}" . "\\paragraph*{%s}")
   ("\\subparagraph{%s}" . "\\subparagraph*{%s}")
   ("\\subsubparagraph{%s}" . "\\subsubparagraph*{%s}")))

(set (mk-local-var 'org-latex-packages-alist)
     '(
       ("utf8" "inputenc")
       ("T1" "fontenc")
       ("" "lmodern")
       ;; ("" "libertine" t)
       ("american" "babel" nil)
       ("" "longtable")
       ("" "tabularx")
       ("" "wrapfig")
       ("" "graphicx")
       ("usenames,dvipsnames,svgnames,table" "xcolor")
       ("" "tikz" t)
       ("" "listings")
       ("" "microtype")
       ("" "makeidx")
       ("" "textcomp")
       ("" "amssymb")
       ("" "amsthm")
       ;; ("" "titlesec")
       ("" "relsize" t)
       ("" "xspace")
       ("" "yfonts")			;fraktur in text
       ("" "fancyvrb")
       ("" "u8chars")
       ("" "hyperref")
       ))
;; (set (mk-local-var 'org-latex-listings) t)

;; Captions above listings as well, to mimic html export.
(set (mk-local-var 'org-latex-caption-above) '(table src-block))

;; Use CUSTOM_ID as labels when specified (to allow \pagerefs)
(set (mk-local-var 'org-latex-prefer-user-labels) t)

;; (defun my-org-latex-url-protect-diese (text backend info)
;;   ""
;;   (when (org-export-derived-backend-p backend 'latex)
;;     (with-temp-buffer
;;       (insert text)
;;       (goto-char (point-min))
;;       (while (search-forward "#" nil t)
;; 	(replace-match "\\#" nil t))
;;       (buffer-string))))
;; (add-hook (mk-local-var 'org-export-filter-link-functions)
;; 	  'my-org-latex-url-protect-diese)

(defun my-org-latex-unnumbered-listings (text backend info)
  ""
  (when (org-export-derived-backend-p backend 'latex)
    (with-temp-buffer
      (insert text)
      (goto-char (point-min))
      (while (re-search-forward "\\lstset.*\\(caption=\\){" nil t)
	(replace-match "title=" nil t nil 1))
      (buffer-string))))
(add-hook (mk-local-var 'org-export-filter-src-block-functions)
	  'my-org-latex-unnumbered-listings)

(defun my-org-latex-fancyvrb (text backend info)
  ""
  (when (org-export-derived-backend-p backend 'latex)
    (with-temp-buffer
      (insert text)
      (goto-char (point-min))
      (while (search-forward "\\begin{verbatim}" nil t)
	(replace-match "\\begin{Verbatim}[]" nil t))
      (goto-char (point-min))
      (while (search-forward "\\end{verbatim}" nil t)
      	(replace-match "\\end{Verbatim}" nil t))
      (goto-char (point-min))
      (while (search-forward "#" nil t) (replace-match "\\#" nil t))
      (goto-char (point-min))
      (while (search-forward "𝔉" nil t) (replace-match "\\textfrak#F$" nil t))
      (goto-char (point-min))
      (while (search-forward "𝔇" nil t) (replace-match "\\textfrak#D$" nil t))
      (buffer-string))))
(add-hook (mk-local-var 'org-export-filter-example-block-functions)
	  'my-org-latex-fancyvrb)

;; ---

(set (mk-local-var 'org-latex-pdf-process)
     '("latexmk -pdf -output-directory=%o %f"))
     ;; '("latexmk -pdf -quiet -output-directory=%o %f"))
     ;; '("pdflatex -interaction nonstopmode -output-directory %o %f"
     ;;   ;; "makeindex %b"
     ;;   "bibtex %b"
     ;;   "pdflatex -interaction nonstopmode -output-directory %o %f"
     ;;   ;; "makeindex %b"			;fix page numbers...
     ;;   "pdflatex -interaction nonstopmode -output-directory %o %f"))

;; ----------------------------------------------------------------------------
;; Customizations for HTML export:

;; ;; XXX global variable modification!
;; (setq org-html-htmlize-output-type 'css)

;; Inserting mathematical macro definitions when exporting to HTML.
(defun my-org-html-insert-mathconf (backend)
  (when (eq backend 'html)
    (set (mk-local-var 'org-html-preamble-format)
         `(("en"                        ;TODO could be a src=".../mathconf.tex"
            ,(format "<script type=\"math/tex\">\n%s\n</script>"
                     (with-temp-buffer (insert-file-contents "etc/mathconf.tex")
                                       (while (search-forward "%" nil t)
                                         (replace-match "%%" nil t))
                                       (buffer-string))))))))
;; XXX Not sure this is or w.r.t local variables:
(add-hook (mk-local-var 'org-export-before-processing-hook)
	  'my-org-html-insert-mathconf)

(defconst my-org-html-math-u8chars-alist
  '(
    ("ℓ" . "\\ell ")
    ("ℕ" . "\\mathbb{N}")
    ("℘" . "\\wp ")
    ("ℚ" . "\\mathbb{Q}")
    ("ℝ" . "\\mathbb{R}")
    ("ℤ" . "\\mathbb{Z}")
    ("←" . "\\leftarrow ")
    ("→" . "\\rightarrow ")
    ("∀" . "\\forall ")
    ("∃" . "\\exists ")
    ("∅" . "\\varnothing ")
    ("∇" . "\\nabla ")
    ("∈" . "\\in ")
    ("∉" . "\\notin ")
    ("∖" . "\\setminus ")
    ("∧" . "\\wedge ")
    ("∨" . "\\vee ")
    ("∩" . "\\cap ")
    ("∪" . "\\cup ")
    ("≜" . "\\stackrel{\\tiny\\Delta}{=}")
    ("≝" . "\\stackrel{\\tiny\\text{def}}{=}")
    ("≠" . "\\neq ")
    ("⊆" . "\\subseteq ")
    ("⊎" . "\\uplus ")
    ("〈" . "\\langle ")
    ("〉" . "\\rangle ")
    ("（" . "\\left(")
    ("）" . "\\right)")
    ("［" . "\\left[")
    ("］" . "\\right]")
    ("｛" . "\\left\\{")
    ("｜" . "\\middle|")
    ("｝" . "\\right\\}")
    ("𝓛" . "\\mathcal{L}")
    ("𝔉" . "\\mathfrak{F}")
    ("𝔏" . "\\mathfrak{L}")
    ("𝔩" . "\\mathfrak{l}")
    ("𝛁" . "\\bnabla ")
    ))
(defconst my-org-html-math-u8chars-regexp
  (regexp-opt (mapcar (lambda (e) (regexp-quote (car e)))
                      my-org-html-math-u8chars-alist) t))
(defun my-org-html-math-macros (text backend info)
  "Replaces some characters by the corresponding math TeX macro (XXX this could
also be done for LaTeX export actually…)."
  (when (org-export-derived-backend-p backend 'html 'latex)
    (with-temp-buffer
      (insert text)
      (goto-char (point-min))
      (while (re-search-forward my-org-html-math-u8chars-regexp nil t)
        (replace-match
         (cdr (assoc (match-string 1) my-org-html-math-u8chars-alist)) nil t))
      (buffer-string))))
(add-hook (mk-local-var 'org-export-filter-latex-fragment-functions)
	  'my-org-html-math-macros)

(defun my-org-html-latex-cmds (text backend info)
  ""
  (when (org-export-derived-backend-p backend 'html)
    (with-temp-buffer
      (insert text)
      (cl-flet ((my-replace (r s)
			    (goto-char (point-min))
			    (while (re-search-forward r nil t)
			      (replace-match s nil t))))
	(my-replace "\\\\relax\s+" "")
	(my-replace "``" "&ldquo;")
	(my-replace "''" "&rdquo;")
	(my-replace "\\>\\\\-\\<" "&shy;"))
      (buffer-string))))
(add-hook (mk-local-var 'org-export-filter-paragraph-functions)
	  'my-org-html-latex-cmds)

(defun my-org-html-paragraph-indents (text backend info)
  "XXX Huge hack to indent the second paragraph in pairs of consecutive
ones (this permits interleaving div elements like source blocks) in between,
without resetting indentation."
  (when (org-export-derived-backend-p backend 'html)
    (with-temp-buffer
      (insert text)
      (goto-char (point-min))
      (while (re-search-forward "\\(</p>\\|\n\\)\n<p>" nil t)
	(replace-match "\\1\n<p class=\"indent\">"))
      (buffer-string))))
(add-hook 'org-export-filter-section-functions
	  'my-org-html-paragraph-indents)

(defun my-org-html-url-style (text backend info)
  ""
  (when (org-export-derived-backend-p backend 'html)
    (with-temp-buffer
      (insert text)
      (goto-char (point-min))
      (if (re-search-forward ">\\(https\?\\|ftp\\|mailto\\|file\\)" nil t)
	  (concat "<span class=\"url\">" text "</span>")
	  text))))
(add-hook (mk-local-var 'org-export-filter-link-functions)
	  'my-org-html-url-style t)

;; Other entities:
(mk-local-var 'org-entities-user)
(defun my-def-entity (n l &optional m h tt)
  (add-to-list 'org-entities-user
	       `(,n ,l ,m ,(or h l) ,(or tt n) ,(or tt n) ,(or tt n))))

(my-def-entity "ie" "\\textit{i.e}\\xspace" nil "<i>i.e</i>" "i.e")
(my-def-entity "eg" "\\textit{e.g}\\xspace" nil "<i>e.g</i>" "e.g")
(my-def-entity "vs" "\\textit{vs.}\\xspace" nil "<i>vs</i>" "vs")
(my-def-entity "apriori" "\\textit{a priori}\\xspace" nil
	       "<i>a priori</i>" "a priori")
(my-def-entity "perse" "\\textit{per se}\\xspace" nil "<i>per se</i>" "per se")
(my-def-entity "wrt" "\\textit{w.r.t}\\xspace" nil "<i>w.r.t</i>" "w.r.t")

(set (mk-local-var 'org-html-footnote-format) "%s")
(set (mk-local-var 'org-html-validation-link) nil)

;; Footnotes:
(set (mk-local-var 'org-html-footnotes-section) 
     "<div id=\"footnotes\"><hr/>
<!-- <h2 class=\"footnotes\">%s </h2> -->
<div id=\"text-footnotes\">
%s
</div>
</div>
")

;; ----------------------------------------------------------------------------
;; Fixing `ox-bibtex' to handle natbib links.

;; TODO fix org-html-latex-fragment (at least).

;; Avoid "openout_any = p" errors when using bibtex2html (see
;; `https://www.lri.fr/~filliatr/bibtex2html/').  (setenv "TMPDIR" ".")
;;
;; To avoid messing with Emacs' environment, I use a custom script also named
;; `bibtex2html' and setting this variable before calling the "real" program.

;; ----------------------------------------------------------------------------
