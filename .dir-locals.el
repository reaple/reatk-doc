((org-mode
  . ((org-confirm-babel-evaluate . nil)
     (org-export-allow-bind-keywords . t)
     (reftex-default-bibliography . ("bib/publications.bib")))))
